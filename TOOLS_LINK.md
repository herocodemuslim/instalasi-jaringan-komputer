# TOOLS INSTALASI JARINGAN KOMPUTER

- Materi JRK Lengkap: http://bit.ly/MateriHIMADIF

- VirtualBox: https://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html
- Debian 7.6: http://kambing.ui.ac.id/iso/debian/7.6.0/i386/iso-dvd/
- Cisco Packet Tracer for Student: http://cs3.calstatela.edu/~egean/cs447/Cisco%20Packet%20Tracer%207.0/



> *Ada setiap harga yang harus dibayar untuk menggapai apa yang kamu inginkan. Dan terkadang, tidak semua hal yang kamu keinginan akan sesuai dengan yang diharapkan.* **Disciplines trumps motivation.**

