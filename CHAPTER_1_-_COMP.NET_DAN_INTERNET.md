# CHAPTER 1: COMP.NET DAN INTERNET

## BEBERAPA PENGERTIAN INTERNET

**Internet** (kependekan dari *interconnection-networking*) adalah seluruh jaringan komputer yang saling terhubung menggunakan standar sistem global [*Transmission Control Protocol/Internet Protocol Suite* (TCP/IP)](http://itjambi.com/apa-itu-tcpip/) sebagai protokol pertukaran paket (*packet switching communication protocol*) untuk melayani miliaran pengguna di seluruh dunia. Rangkaian internet yang terbesar dinamakan **Internet**. Cara menghubungkan rangkaian dengan kaidah ini dinamakan *internetworking* (“antarjaringan”). 

**Internet** adalah sebuah jaringan komputer yang terdiri dari berbagai macam ukuran dan jenis jaringan komputer di seluruh dunia. Jaringan-jaringan komputer ini saling berhubungan dan berkomunikasi satu sama lain melalui bantuan telepon dan satelit, yang digunakan untuk keperluan pemerintahan, pendidikan, perdagangan, ilmu pengetahuan dan perorangan.

*Simple Term*: Internet (Interconnection-networking) jaringan komputer yang terhubung satu sama lain diseluruh dunia.

### TUGAS BACA

- [Apa, Sejarah, Manfaat, dan Kerugian Internet](https://hadewa.com/2018/01/08/apa-itu-internet/)



### REFERENSI

- http://itjambi.com/apa-itu-internet/
- https://hadewa.com/2018/01/08/apa-itu-internet/